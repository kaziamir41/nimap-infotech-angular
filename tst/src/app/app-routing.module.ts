import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';

import { BookDComponent } from './book-d/book-d.component';
import { AmirComponent } from './amir/amir.component';
import { GetbookComponent } from './getbook/getbook.component';


const routes: Routes = [
  {
    path:'',
    component:AmirComponent,
    pathMatch:"full"
  },
  {
    path:'amir',
    component:AmirComponent
  },
  {
  path:'home',
  component:HomeComponent
},

{
path:'book',
component:BookDComponent
},
{
  path:'getbook',
component:GetbookComponent
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
