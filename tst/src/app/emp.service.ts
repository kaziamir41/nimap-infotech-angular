import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { bookinter } from './book';

@Injectable({
  providedIn: 'root'
})
export class EmpService {
public url="http://localhost:3000/book"
  constructor(private http:HttpClient) { }
  array():Observable<bookinter[]>{
    return this.http.get<bookinter[]>(this.url);
  }
    
  parray(a):Observable<bookinter[]>{
    return this.http.post<bookinter[]>(this.url,a)
  }
}
