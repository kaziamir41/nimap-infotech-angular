import { Component, OnInit } from '@angular/core';
import { EmpService } from '../emp.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-book-d',
  templateUrl: './book-d.component.html',
  styleUrls: ['./book-d.component.css']
})
export class BookDComponent implements OnInit {
public book;
  constructor(private emp:EmpService,public ff:FormBuilder) { }
formData:FormGroup;
  ngOnInit(): void {
    this.formData=this.ff.group({
      id:["",Validators.required],
      name:["",Validators.required],
      author:["",Validators.required]
    })
  }
// get(){
//   this.emp.array().subscribe(data=>this.book=data)
// }

click(){
  if(this.formData.valid)
  {
  this.emp.parray(this.formData.value).subscribe(
    res=>{
      return alert("SUCCESSFULLY ADDED VALUE")
  

    }
  )
}
}
}
